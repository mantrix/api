# Signup

### Create User

1. Use firebase client sdk to create a user. 

- [Android](https://firebase.google.com/docs/auth/android/password-auth#create_a_password-based_account)
- [iOS](https://firebase.google.com/docs/auth/ios/password-auth)
- [Javascript](https://firebase.google.com/docs/auth/web/password-auth)

This will return the user object (with `uid` inside) created by firebase. After creating the user, send the user information to the server to create a "user info" collection.

| Route | Method | Payload |
| ----- | ------ | ------- |
| `/user/create` | POST | [See](#create-user-payload) |

#### Create user payload

```JSON
{
  "uid": "", // the uid from firebase
  "email": "",
  "name": {
    "firstName": "",
    "middleName": "",
    "lastName": ""
  },
  "mobileNo": ""
}
```