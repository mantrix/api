# Analytics

- [System Analytics](#system-analytics)
- [User Analytics](#user-analytics)

### System Analytics

```JSON
"analytics/system-analytics": {
  "[module]": { // module is, project, factors, etc. Anything that we need keep track of the count.
    "data": {
      "[md5 hashed utc]": 45, // say this is mon
      "[md5 hashed utc]": 5, // say this is tues
      "[md5 hashed utc]": 30, // say this is wed
      "[md5 hashed utc]": 20 // say this is thur
    },
    "total": 100
  }
}
```

### User Analytics

```JSON
"analytics/user-analytics": {
  "[uid]": {
    "[module]": { // module is, project, factors, etc. Anything that we need keep track of the count.
      "data": {
        "[utc date]": 45, // say this is mon
        "[utc date]": 5, // say this is tues
        "[utc date]": 30, // say this is wed
        "[utc date]": 20 // say this is thur
      },
      "total": 100
    }
  }
}
```