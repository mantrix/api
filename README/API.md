# API

> Bast path: `https://us-central1-playground-5431f.cloudfunctions.net`

## Table of Contents

1. [Signup](#signup)

## Signup

> POST: `[base path]/user/create`

**Payload**
```JSON
{
  "uid": "Fp92trpE4NWtJrO3UwMMlHx6aPy1",
  "email": "eng1@test.com",
  "name": {
    "firstName": "Joff",
    "middleName": "",
    "lastName": "Tiquez"
  },
  "mobileNo": "9165008231"
}
```

**Fields**
> To see all fields see README.md

| key | type | enum/sample | required | description |
| --- | ---- | ---- | -------- | ----------- |
| `uid` | String | -- | yes | Generated thru firebase auth |
| `email` | String | -- | yes | User's email used to create firebase account. |
| `name` | Object | [see](#name) | yes | -- |
| `mobileNo` | String | -- | yes | Mobile no |

#### name
```JSON
"name": {
  "firstName": "Erick",
  "middleName": "",
  "lastName": "Sumugat"
}
```

[Back to top ↑](#table-of-contents)