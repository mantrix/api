import CryptoJS from 'crypto-js';
import crypto from 'crypto';
import * as functions from 'firebase-functions';

const SECRET = functions.config().secrets.auth_secret;

export const enc = (plainText) => {
  console.log(SECRET);
  return crypto.createCipher('aes-256-ctr', SECRET).update(plainText, 'utf8', 'hex');
}

export const dec = (cipherText) => {
  return crypto.createDecipher('aes-256-ctr', SECRET).update(cipherText, 'hex', 'utf8');
}