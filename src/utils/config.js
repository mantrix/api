import config from '../.config';

export const isProd = () => config.isProd;

export const version = config.VERSION;

export const fb = {
  serviceAccount: () => config[isProd() ? 'PROD' : 'TEST'].firebase.serviceAccount,
  databaseURL: () => config[isProd() ? 'PROD' : 'TEST'].firebase.databaseURL,
}