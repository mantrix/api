export default (snap) => {
  const arr = [];

  snap.forEach(child => {
    arr.push({
      uid: child.key,
      ...child.val()
    })
  });

  return arr;
}