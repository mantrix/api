// import { format } from 'date-fns';
import moment from 'moment-timezone';

export const today = (tz = 'Asia/Manila') => {
  return moment().tz(tz).format('MM-DD-YYYY');
}

export const todayUTC = (replace) => {
  const date = new Date();
  const utc = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)).toISOString();

  if(replace) {
    return utc.replace('.', '::');
  }

  return utc;
}

// export const todayUTC = () => {
//   return {
//     short: () => {
//       const date = new Date();
//       const utc = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)).toISOString();
//       return utc.substring(0,10);
//     },
//     full: () => {
//       const date = new Date();
//       return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)).toISOString();
//     }
//   }
// }