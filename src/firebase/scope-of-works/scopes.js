import { ref } from '../exports';

export const removeProjectScopeOfWorks = async (project) => {
  await ref.child('project-scope-of-works')
    .child(project)
    .set(null);
}

export const removeProjectProperties = async (project) => {
  await ref.child('project-properties')
    .child(project)
    .set(null);
}

export const removeProjectFactors = async (project) => {
  await ref.child('project-factors')
    .child(project)
    .set(null);
}