import { auth, ref } from '../exports';
import { format, addDays } from 'date-fns'

export const _signupUser = async (data) => {
  
  const authUser = await auth.createUser({
    email: data.email,
    emailVerified: false,
    // phoneNumber: `+63${data.mobileNo}`,
    password: data.password,
    displayName: `${data.name.firstName} ${data.name.lastName}`,
    // photoURL: '',
    disabled: false
  });

  delete data.password;

  await _createUser(authUser, data);
}

export const _createUser = async (user, data) => {
  const userRef = ref.child('users');

  const snap = await userRef.child('data')
    .child(user.uid)
    .once('value');

  const configSnap = await ref.child('_config')
    .child('trialLimitDays')
    .once('value');
  
  if(snap.exists())
    throw {code: 400, message: `User with uid ${user.uid} already exists.`}

  delete data.password;

  const subscription = {
    trial: true,
    trialEndAt: format(addDays(new Date(), configSnap.val() || 15), 'MM-DD-YYYY'),
    ...data.subscription
  }
  
  const websiteEnabled = data.subscription.plan !== 'free';

  delete data.subscription;

  await userRef.child('data')
    .child(user.uid)
    .set({
      createdAt: new Date().getTime(),
      url: '',
      websiteEnabled,
      ...data,
      subscription
    });

  await userRef.child('total')
    .transaction(total => {
      if(!total) return 1;
      return ++total;
    });
}

export const _getUserByIUD = async (uid) => {
  return await auth.getUser(uid);
}

export const _addProjectToUser = async ({uid, project}) => {
  await ref.child()
}