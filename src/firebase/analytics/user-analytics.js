import { ref } from '../exports';
import { todayUTC, today } from '../../utils/date';

const pathData = `analytics/user-analytics/:uid/:module/data/:date`;
const pathTotal = `analytics/user-analytics/:uid/:module/total`;

/**
 * @param {String} uid The user uid
 * @param {String} module Module name e.g. projects, users
 * @param {Boolean} upward Direction of transaction, if true, add 1 else subtract 1
 */
export const recordUserAnalytics = (uid, module, upward = true) => {
  try {
    ref.child(
      pathData.replace(/:uid/, uid)
        .replace(/:module/, module)
        .replace(/:date/, today)
    ).transaction(total => {
      if(!total && upward) return 1;
      if(!total && !upward) return 0;
      if(upward)
        return ++total;
      else 
        return --total;
    });

    ref.child(
      pathTotal.replace(/:uid/, uid)
        .replace(/:module/, module)
    ).transaction(total => {
      if(!total && upward) return 1;
      if(!total && !upward) return 0;
      if(upward)
        return ++total;
      else 
        return --total;
    });
  } catch (e) {
    throw e;
  }
}