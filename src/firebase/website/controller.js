import { ref } from '../exports';

export const _getWebsite = async (url) => {
  const snap = await ref.child('users')
    .child('data')
    .orderByChild('url')
    .equalTo(url)
    .once('value');

  let user = {};

  snap.forEach(child => {
    user = {
      uid: child.key,
      ...child.val()
    }
  });

  console.log('website:user', user);

  if(user.websiteEnabled) {
    return user;
  } else {
    throw {code: 404, message: 'User not found!'}
  }
}