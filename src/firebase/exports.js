import * as firebase from 'firebase-admin';
import { isProd, fb, version } from '../utils';

firebase.initializeApp({
  credential: firebase.credential.cert(fb.serviceAccount()),
  databaseURL: fb.databaseURL()
});

export const auth = firebase.auth();
export const ref = firebase.database().ref(version);