import { ref } from '../exports';

export const removeSharedFactors = async (project) => {
  await ref.child('shared-factors')
    .child(project)
    .set(null);
}