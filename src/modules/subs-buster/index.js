import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import express from 'express';
import cors from 'cors';
import { ref } from '../../firebase';
import arrayfy from '../../utils/arrayfy';
import moment from 'moment-timezone';
import { forEachAsync } from 'forEachAsync';
const TIME_ZONE = 'Asia/Manila';

function correctDate (date) {
  return `${date.split('-')[2]}-${date.split('-')[0]}-${date.split('-')[1]}`;
}

const app = express();
app.use(cors({ origin: true }));
app.get('/bust-now', async (req, res) => {
  try {
    const snap = await ref.child('users')
      .child('data')
      .orderByChild('subscription/subscribed')
      .equalTo(true)
      .once('value');

    let today = moment.tz(TIME_ZONE).valueOf();
    let subsUser = arrayfy(snap);
    let endingSubs = subsUser.filter(user => moment.tz(user.subscription.end, TIME_ZONE).valueOf() < today);
    forEachAsync(subsUser, (n, user) => {
      (async (n, user) => {
        let subsEnd = moment.tz(user.subscription.end, TIME_ZONE).valueOf();
        today = moment.tz(TIME_ZONE).valueOf();;
        
        let subsRef = ref.child('users')
          .child('data')
          .child(user.uid)
          .child('subscription');
        
        if (req.query.testDate) {
          subsEnd = req.query.testDate;
        }

        // SUBS EXPIRES
        if (subsEnd < today) {
          await subsRef.child('subscribed').set(false);
          await subsRef.child('paid').set(false);
          await subsRef.child('plan').set('free');
          //
          await subsRef.child('trial').set(false);
        }

        n();
      })(n, user);
    }).then(() => {
      res.send(`${subsUser.length} subscribed users. | ${endingSubs.length} ending subscription/s today.`);
    });
  } catch (e) {
    console.error(e);
    res.send(e);
  }
});

export const subsBuster = functions.https.onRequest(app);