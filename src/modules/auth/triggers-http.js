import * as functions from 'firebase-functions';
import express from 'express';
import cors from 'cors';
import { checkRestrictions } from './auth';
import { authHttp } from '../auth-http';

const app = express();
app.use(cors({ origin: true }));
app.post('/restrictions', authHttp, checkRestrictions);

export const auth = functions.https.onRequest(app);