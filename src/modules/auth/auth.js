import { ref } from '../../firebase';

const validPlatforms = ['android', 'desktop', 'ios'];

export const checkRestrictions = async (req, res) => {
  try {
    const { uid, deviceId, platform } = req.body;

    if(!(validPlatforms.indexOf(platform) > -1)) {
      res.status(400).send({message: `Invalid platform must be one of ${validPlatforms}`});
      return;
    }

    const userDeviceRef = ref.child('user-devices')
      .child(uid)
      .child(platform)

    const snap = await userDeviceRef.once('value');

    const existingDeviceId = snap.val();

    if(!existingDeviceId) {
      await userDeviceRef.set(deviceId);
      res.status(200).send({message: 'device-allowed'});
    } else {
      if(existingDeviceId === deviceId) {
        res.status(200).send({message: 'device-allowed'});
      } else {
        res.status(403).send({message: 'device-not-allowed'});
      }
    }

  } catch (e) {
    res.status(500).send(e.message);
  }
}