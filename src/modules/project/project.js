import { ref } from '../../firebase';

/**
 * @param {Boolean} upward Direction of transaction, if true, add 1 else subtract 1
 */
export const updateTotal = async (upward = true) => {
  await ref.child('projects')
    .child('total')
    .transaction(total => {
      if(!total && upward) return 1;
      if(!total && !upward) return 0;
      if(upward)
        return ++total;
      else 
        return --total;
    });
}

export const updateProjectSearchIndex = async ({project, data}) => {
  await ref.child('projects')
    .child('data')
    .child(project)
    .child('nameSearchIndex')
    .set(data.toLowerCase())
}