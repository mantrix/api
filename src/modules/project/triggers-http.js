import * as functions from 'firebase-functions';
import express from 'express';
import cors from 'cors';
import { createNewUser } from './project';

const app = express();
app.use(cors({ origin: true }));

export const project = functions.https.onRequest(app);