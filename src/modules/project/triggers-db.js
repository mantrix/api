import { database } from 'firebase-functions';
import { updateTotal, updateProjectSearchIndex } from './project';
import { addProjectToUser, removeProjectFromUser, updateUserProjectTotal } from '../user/user';
import { recordUserAnalytics, recordSystemAnalytics } from '../../firebase/analytics';
import { removeSharedFactors } from '../../firebase/factors';
import { removeProjectScopeOfWorks, removeProjectProperties, removeProjectFactors } from '../../firebase/scope-of-works';
import { version } from '../../utils';

const MODULE = '#PROJECTS';
const base = `${version}/projects`;
const paths = {
  data: `${base}/data/{project}`,
  total: `${base}/total`
}

export const createProject = database.ref(paths.data)
  .onCreate(async (snap, context) => {
    try {
      const data = snap.val();
      const { project } = context.params;
      const { createdBy:uid } = data;

      // TODO: add validation
      
      await updateTotal();
      await addProjectToUser({uid, project});
      await updateUserProjectTotal(uid);
      // await recordSystemAnalytics('projects');
      // await recordUserAnalytics(uid, 'projects');

      console.log('#PROJECT - CREATED!');
    } catch (e) {
      console.error(MODULE, '#CREATE', e);
    }
  });

export const updateProjectName = database.ref(`${paths.data}`)
  .onUpdate(async (snap, context) => {
    try {
      const data = snap.val();
      const { project } = context.params;

      console.log(data)
      console.log(project)
      
      await updateProjectSearchIndex({project, data});

      console.log('#PROJECT - NAME UPDATED!');
    } catch (e) {
      console.error(MODULE, '#UPDATE NAME', e);
    }
  });

export const deleteProject = database.ref(paths.data)
  .onDelete(async (snap, context) => {
    try {
      const data = snap.val();
      const { project } = context.params;
      const { createdBy:uid } = data;
      
      console.log(uid);
      console.log(project);

      await updateTotal(false);
      await removeProjectFromUser({uid, project});
      await updateUserProjectTotal(data.createdBy, false);
      await removeSharedFactors(project);
      await removeProjectScopeOfWorks(project);
      await removeProjectFactors(project);
      await removeProjectProperties(project);
      await recordSystemAnalytics('projects', false);
      await recordUserAnalytics(uid, 'projects', false);

      console.log('#PROJECT - DELETED!');
    } catch (e) {
      console.error(MODULE, '#DELETE', e);
    }
  });