import { _getWebsite } from '../../firebase/website';

export const getWebsite = async (req, res) => {
  try {
    const user = await _getWebsite(req.params.url);
    const data = {
      ...user
    };
    
    user.name && user.name.middleName ? data.name.middleName = `${user.name.middleName.substr(0,1).toUpperCase()}.` : data.name.middleName = '';

    if(user.name && user.name.prefix) {
      if(user.name.prefix === 'Engineer')
        data.name.prefix = 'Engr.';
      if (user.name.prefix === 'Architect')
        data.name.prefix = 'Arch.';
    }
    
    const educations = [];
    for (const key in user.educations) {
      educations.push(user.educations[key])
    }
    data.educations = educations;

    const services = [];
    for (const key in user.services) {
      services.push(user.services[key])
    }
    data.services = services;

    res.render('index', data);
  } catch (e) {
    if(e.code === 404)
      res.render('not-found');
    else
      res.render('error');
  }
}