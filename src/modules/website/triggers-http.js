import * as functions from 'firebase-functions';
import express from 'express';
import cors from 'cors';
import { getWebsite } from './controller';
import mustacheExpress from 'mustache-express';

const app = express();
app.engine('html', mustacheExpress());
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(cors({ origin: true }));
app.get('/:url', getWebsite);

export const website = functions.https.onRequest(app);