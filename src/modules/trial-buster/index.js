import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import express from 'express';
import cors from 'cors';
import { ref } from '../../firebase';
import arrayfy from '../../utils/arrayfy';
import moment from 'moment-timezone';
import { forEachAsync } from 'forEachAsync';
const TIME_ZONE = 'Asia/Manila';

function correctDate (date) {
  return `${date.split('-')[2]}-${date.split('-')[0]}-${date.split('-')[1]}`;
}

const app = express();
app.use(cors({ origin: true }));
app.get('/bust-now', async (req, res) => {
  try {
    const snap = await ref.child('users')
      .child('data')
      .orderByChild('subscription/trial')
      .equalTo(true)
      .once('value');

    let today = moment.tz(TIME_ZONE).valueOf();
    let usersOnTrial = arrayfy(snap);
    let endingTrial = usersOnTrial.filter(user => moment.tz(correctDate(user.subscription.trialEndAt), TIME_ZONE).valueOf() < today);

    forEachAsync(usersOnTrial, (n, user) => {
      (async (n, user) => {
        let trialEnd = moment.tz(correctDate(user.subscription.trialEndAt), TIME_ZONE).valueOf()
        let isSubscribed = user.subscription.subscribed;
        today = moment.tz(TIME_ZONE).valueOf();
        
        let subsRef = ref.child('users')
          .child('data')
          .child(user.uid)
          .child('subscription');

        if (req.query.testDate) {
          trialEnd = req.query.testDate;
        }

        // REMOVE TRIAL
        console.log('trial end ===>', user.subscription.trialEndAt, trialEnd);
        console.log('today ===>', today);
        if (trialEnd < today) {
          await subsRef.child('trial').set(false);
          if (!isSubscribed) {
            await subsRef.child('paid').set(false);
            await subsRef.child('plan').set('free');
          }
        }

        n();
      })(n, user);
    }).then(() => {
      res.send(`${usersOnTrial.length} on trial. | ${endingTrial.length} ending trial/s today.`);
    });
  } catch (e) {
    res.send(e);
  }
});

export const trialBuster = functions.https.onRequest(app);