import * as functions from 'firebase-functions';
import express from 'express';
import cors from 'cors';
import { createUser, signupUser, deleteUsers } from './user';
import { authHttp } from '../auth-http';

const app = express();
app.use(cors({ origin: true }));
// API will create the user
app.post('/signup', signupUser);
// app.get('/delete/:secret', deleteUsers);
// Front end will create the user
app.post('/create', authHttp, createUser);

export const user = functions.https.onRequest(app);