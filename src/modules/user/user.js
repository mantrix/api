import { _signupUser, _createUser, _getUserByIUD } from '../../firebase/user';
import { ref, auth } from '../../firebase';
import forEachAsync from 'forEachAsync';

const userProjectsDataPath = `user-projects/:uid/data/:project`;
const userProjectsTotalPath = `user-projects/:uid/total`;

async function validateSignupUserPayload(body) {
  try {
    const { email, password, name, mobileNo, subscription } = body;
    if(!email) 
      throw {code: 400, message: `email is required.`}
    
    if(!password)
      throw {code: 400, message: `password is required.`}
    
    if(!name) 
      throw {code: 400, message: `name object is required.`}
    
    if(!name.firstName) 
      throw {code: 400, message: `firstName is required.`}
    
    if(!name.lastName) 
      throw {code: 400, message: `lastName is required.`}
    
    if(!mobileNo) 
      throw {code: 400, message: `mobileNo is required.`}
    
    if(!subscription && !subscription.plan) {
      throw {code: 400, message: `subscription is required.`}
    }
    
    return true;
  } catch (e) {
    throw e;
  }
}

async function validateCreateUserPayload(data) {
  try {
    const { name, mobileNo, subscription } = data;
    if(!name) 
      throw {code: 400, message: `name object is required.`}
    
    if(!name.firstName) 
      throw {code: 400, message: `firstName is required.`}
    
    if(!name.lastName) 
      throw {code: 400, message: `lastName is required.`}
    
    if(!mobileNo) 
      throw {code: 400, message: `mobileNo is required.`}
    
    if(!subscription && !subscription.plan) {
      throw {code: 400, message: `subscription is required.`}
    }
    
    return true;
  } catch (e) {
    throw e;
  }
}

export const signupUser = async (req, res) => {
  try {
    await validateSignupUserPayload(req.body);
    await _signupUser(req.body);

    res.send('OK');
  } catch (e) {
    console.log('eeeeerror', e);
    let code = e.code;
    if(typeof code === 'string')
      code = 500;
    res.status(code).send(e.message);
  }
}

export const createUser = async (req, res) => {
  try {
    const { user, data } = req.body;
    console.log(req.body);
    await validateCreateUserPayload(req.body.data)
    await _createUser(user, data);
    res.send('OK');
  } catch (e) {
    console.log('', e);
    let code = e.code;

    if(typeof code === 'string')
      code = 500;

    if (code >= 100 && code < 600)
      res.status(code).send(e.message);
    else
      res.status(500).send(e.message);
  }
}

// export const getUser = async () => {
//   try {
//     // TODO: Get
//   } catch (e) {
//     throw e;
//   }
// }

// export const updateUser = async () => {
//   try {
//     // TODO: Update
//   } catch (e) {
//     throw e;
//   }
// }

// export const deleteUser = async () => {
//   try {
//     // TODO: Delete
//   } catch (e) {
//     throw e;
//   }
// }

export const addProjectToUser = async ({uid, project}) => {
  try {
    await ref.child(
      userProjectsDataPath.replace(/:uid/, uid)
        .replace(/:project/, project)
    ).set(true);
  } catch (e) {
    throw e;
  }
}

export const removeProjectFromUser = async ({uid, project}) => {
  try {
    await ref.child(
      userProjectsDataPath.replace(/:uid/, uid)
        .replace(/:project/, project)
    ).set(null);
  } catch (e) {
    throw e;
  }
}

export const updateUserProjectTotal = async (uid, upward = true) => {
  try {
    await ref.child(userProjectsTotalPath.replace(/:uid/, uid))
      .transaction(total => {
        if(!total && upward) return 1;
        if(!total && !upward) return 0;
        if(upward)
          return ++total;
        else 
          return --total;
      });
  } catch (e) {
    throw e;
  }
}

export const deleteUsers = async (req, res) => {
  try {
    
    if(req.params.secret !== 'chickendinner2019') {
      res.status(403).send('You have no power here.');
      return;
    }

    const { users } = await auth.listUsers();

    forEachAsync(users, (n, e) => {
      (async (n, e) => {
        setTimeout(async () => {
          await auth.deleteUser(e.uid);
          console.log(e.uid, 'deleted');
          n();
        }, 1000);
      })(n, e);
    });

    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send(e);
  }
}