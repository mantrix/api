# Deployment

## Clone

Clone the project from bitbucket.

## Install dependencies

Make sure that `yarn` and `node.js` (both latest version) are available in your machine.

Make sure that all firebase essentials are installed in the machine, like `firebase command line tools`

To install dependencies run `yarn insall` or simply just `yarn` in your terminal.

## Configurations

Provide the `.config.json`. These are the secret keys required by the server. Ask the management for this file and add it to the root directory of this project.

## Build and deploy to TEST

Make sure that the `isProd` field in the `.config.json` is set to `false`.

To build the project just run `yarn build`.

To deploy all available firebase functions - run `firebase deploy --only functions -P [the firebase test project id]`.

To deploy a single/specific firebase functions - run `firebase deploy --only functions:samepleFunctionName -P [the firebase test project id]`.

## Build and deploy to PROD

Make sure that the `isProd` field in the `.config.json` is set to `true`.

To build the project just run `yarn build`.

To deploy all available firebase functions - run `firebase deploy --only functions -P [the firebase prod project id]`.

To deploy a single/specific firebase functions - run `firebase deploy --only functions:samepleFunctionName -P [the firebase prod project id]`.

# Data Model

- [Factors](#factors)
- [Project](#project)
- [Scopes of work](#scopes-of-work)
- [User Projects](#user-projects)
- [Users](#users)
- [Project Unit Cost](#project-unit-cost-analysis)

## Factors

Formula factors.

```JSON
"factor-defaults": {
  "[push-key]": {}
}
```

```JSON
"factors-custom": {
  "[uid]": {
    "data": {
      "[push-key]": {}
    },
    "total": 1
  }
}
```


## Project

List of all projects. Projects will be created using firebase SDK, not REST to the API to maximize offline capability of firebase.

```JSON
"projects": {
  "data": {
    "[project-id-1]": {
      "createdAt": 12312312313,
      "startedAt": 12312311231,
      "completedAt": 12312312313,
      "privacy": "public", // [public, private]
      "favorited": false,
      "name": "",
      "description": "",
      "client": "",
      "photos": {
        "[push-key-1]": "https://...",
        "[push-key-2]": "https://...",
        "[push-key-3]": "https://..."
      },
      "team": {
        "lead": {}
      },
      "createdBy": "[uid]", // uid of the user who created the project
      "factors": {
        "[push-key]": {
          "name": "Concrete Works",
          "foo": 12,
          "bar": 23
        },
        "[push-key]": {},
        "[push-key]": {},
        "[push-key]": {}
      }
    },
    "[project-id-2]": {...},
    "[project-id-3]": {...}
  },
  "total": 3
}
```

```JSON
"project-scope-of-works": {
  "[project-id]": {
    "data": {
      "[push-key-1]": {
        "name": "",
        "description": "",
        "category": "",
        "location": {
          "floorNo": "1",
          "part": "foo" // ceiling
        },
        "cost": 23000
      },
      "[push-key-2]": {
        "name": "",
        "description": "",
        "category": "",
        "location": {
          "floorNo": "1",
          "part": "foo" // wall
        },
        "cost": 37466
      },
      "[push-key-3]": {
        "name": "",
        "description": "",
        "category": "",
        "location": {
          "floorNo": "1",
          "part": "foo" // floor
        },
        "cost": 5999
      }
    },
    "total": 3
  }
}
```

## Scopes of work

The default scopes of works that users can choose/use.

```JSON
"scopes-of-work-defaults": {
  "[push-key]": {
    "name": "Tile works",
    "description": "Blah blah for floor tiling works",
    "category": {}
  }
}
```

## Users

List of users.

```JSON
"users": {
  "data": {
    "[user-id-1]": {
      "createdAt": 123123123,
      "searchIndexFirstName": "", // lowercase version of name.firstName
      "searchIndexLastName": "", // lowercase version of name.middleName
      "searchIndexMiddleName": "", // lowercase version of name.lastName
      "name": {
        "firstName": "",
        "middleName": "",
        "lastName": ""
      },
      "bio": "",
      "email": "",
      "sex": "", // M, F
      "licenseNo": "",
      "mobileNo": "",
      "picURL": "",
      "url": "eng-erick",
      "educations": {
        "[push-key-1]": {
          "degree": "",
          "school": "",
          "start": 2009,
          "end": 2013
        }
      },
      "services": {
        "[push-key-1]": {
          "name": "",
          "description": "",
          "priceRange": "200 - 500"
        }
      },
      // modify via REST
      "subscription": {
        "plan": "free", // [free, basic, premium]
        "paid": true,
        "start": 123123123,
        "end": 123123123,
        "paymentMethod": "", // [pdc, cc, cash]
        "interval": ""
      },
      "contacts": {
        "workEmail": "",
        "phone": {
          "work": "",
          "workMobile": ""
        },
        "social" : {
          "facebook": "",
          "twitter": "",
          "linkedIn": "",
          "googlePlus": "",
          "other": ""
        }
      },
    },
    "[user-id-2]": {...},
    "[user-id-3]": {...},
    "[user-id-4]": {...},
    "[user-id-5]": {...}
  },
  "total": 5
}
```

## User Projects

List of user owned projects.

```JSON
"user-projects": {
  "[user-id]": {
    "data": {
      "[project-id-1]": true,
      "[project-id-2]": true,
      "[project-id-3]": true,
      "[project-id-4]": true
    },
    "total": 4
  }
}
```

## Project unit cost analytis

```JSON
"project-unit-cost-analyses": {
  "[project-id]": {
    "data": {
      "[push-key]": {
        
        "unitCostScopeOfWorks": "",
        "unitCostCategory": "",
        "unitCostQuantity": "",
        "unitCostUnit": "",

        "materials": {
          "data": {
            "[push-key]": {
              "description": "",
              "quantity": 10,
              "unit": "bag", // cu.m, etc.
              "price": 10,
            }
          }
        },
        "labor": {
          "[push-key]": {
            "type": "man-hours", // man-hours, team-hours
            "section": "Concrete Mixing",
            "quantityOfWork": 10,
            
            "outputRate": 10,
            "totalSkilled": 20,
            "totalLaborer": 10
          }
        },
        "equipments": {
          "[push-key]": {
            "description": "",
            "quantity": "",
            "hourlyRate": 100,
            "hours": ""
          }
        }
      }  
    },
    "total": 10
  }
}

```

## User Devices

```JSON
"user-devices": {
  "[uid]": {
    "android": "[id]",
    "desktop": "[id]",
    "ios": "[id]",
  }
}
```

```JSON
[
  {
    "type": "man-hours", // man-hours, team-hours
    "section": "Concrete Mixing",
    "quantityOfWork": 10,
    "workers": [
      {
        "designation": "",
        "noOfWorkers": 0,
        "noOfHours": 0,
        "hourlyRate": 0
      }
    ]
  },
  {
    "type": "man-hours", // man-hours, team-hours
    "section": "Concrete Mixing 2",
    "quantityOfWork": 10,
    "workers": [
      {
        "designation": "",
        "noOfWorkers": 0,
        "noOfHours": 0,
        "hourlyRate": 0
      }
    ]
  }
]
```